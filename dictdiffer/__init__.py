# This file is part of Dictdiffer.
#
# Copyright (C) 2013 Fatih Erikli.
# Copyright (C) 2013, 2014, 2015, 2016 CERN.
# Copyright (C) 2017-2019 ETH Zurich, Swiss Data Science Center, Jiri Kuncar.
#
# Dictdiffer is free software; you can redistribute it and/or modify
# it under the terms of the MIT License; see LICENSE file for more
# details.

"""Dictdiffer is a helper module to diff and patch dictionaries."""

from copy import deepcopy
from typing import (
    Dict,
    Hashable,
    List,
    Mapping,
    Optional,
    Sequence,
    Set,
    Tuple,
    Union,
    cast,
)
from collections import (
    MutableMapping,
    MutableSequence,
    MutableSet,
    Iterable,
)

from .utils import EPSILON, PathLimit, are_different, dot_lookup

M = Union[MutableMapping, Sequence, Set]

ADD = "add"
REMOVE = "remov"
CHANGE = "change"


# def diff(first:Dict,second:Dict, node:Optional[Hashable]=None,ignore:Optional[Sequence[str]]=None,path_limit:Optional[Union[PathLimit,List[Tuple]]]=None,expand:bool=False,tolerance:float=None,dot_notation=True):
def diff(
    first: Dict,
    second: Dict,
    node: Optional[Hashable] = None,
    ignore: Union[Iterable, List] = None,
    path_limit: Optional[Union[PathLimit, Tuple[str, ...]]] = None,
    expand=False,
    tolerance: float = None,
    dot_notation=True,
):
    if not tolerance:
        tolerance = EPSILON
    elif path_limit and not isinstance(path_limit, PathLimit):
        path_limit = PathLimit(path_limit)

    # path_limit: PathLimit
    if type(ignore) == Iterable:

        def _process_ignore_value(value):
            if isinstance(value, int):
                return (value,)
            elif isinstance(value, list):
                return tuple(value)
            elif not dot_notation and isinstance(value, str):
                return (value,)
            return value

        ignore = type(ignore)(_process_ignore_value(value) for value in ignore)

    def dotted(node, default_type=list):
        """Return dotted notation."""
        if dot_notation and all(
            map(lambda x: isinstance(x, str) and "." not in x, node)
        ):
            return ".".join(node)
        else:
            return default_type(node)

    def _diff_recursive(_first: M, _second: M, _node=None):
        _node = _node or []

        dotted_node = dotted(_node)
        addition = None
        deletion = None
        differ = False
        intersection = None
        if isinstance(_first, MutableMapping) and isinstance(_second, MutableMapping):
            # dictionaries are not hashable, we can't use sets
            def check(key):
                """Test if key in current node should be ignored."""
                return ignore is None or (
                    dotted(_node + [key], default_type=tuple) not in ignore
                    and tuple(_node + [key]) not in ignore
                )

            intersection = [k for k in _first if k in _second and check(k)]
            addition = [k for k in _second if k not in _first and check(k)]
            deletion = [k for k in _first if k not in _second and check(k)]

            differ = True

        elif isinstance(_first, Sequence) and isinstance(_second, Sequence):
            len_first = len(_first)
            len_second = len(_second)

            intersection = list(range(0, min(len_first, len_second)))
            addition = list(range(min(len_first, len_second), len_second))
            deletion = list(reversed(range(min(len_first, len_second), len_first)))

            differ = True

        elif isinstance(_first, MutableSet) and isinstance(_second, MutableSet):
            # Deep copy is not necessary for hashable items.
            addition = _second - _first
            if len(addition):
                yield ADD, dotted_node, [(0, addition)]
            deletion = _first - _second
            if len(deletion):
                yield REMOVE, dotted_node, [(0, deletion)]

            return  # stop here for sets

        if differ:

            # Compare if object is a dictionary or list.
            #
            # NOTE variables: intersection, addition, deletion contain only
            # hashable types, hence they do not need to be deepcopied.
            #
            # Call again the parent function as recursive if dictionary have
            # child objects.  Yields `add` and `remove` flags.
            assert isinstance(_first, (Mapping, Sequence))
            assert isinstance(_second, (Mapping, Sequence))
            for key in intersection:
                # if type is not changed,
                # callees again diff function to compare.
                # otherwise, the change will be handled as `change` flag.

                if (
                    path_limit
                    and isinstance(path_limit, PathLimit)
                    and path_limit.path_is_limit(_node + [key])
                ):
                    yield CHANGE, _node + [key], (
                        deepcopy(_first[key]),
                        deepcopy(_second[key]),
                    )
                else:
                    recurred = _diff_recursive(
                        _first[key],
                        _second[key],
                        _node=_node + [key],
                    )

                    for diffed in recurred:
                        yield diffed

            if addition:
                assert isinstance(path_limit, PathLimit)
                if path_limit:
                    collect = []
                    collect_recurred = []
                    for key in addition:
                        if not isinstance(
                            _second[key], (MutableMapping, MutableSequence, MutableSet)
                        ):
                            collect.append((key, deepcopy(_second[key])))
                        elif path_limit.path_is_limit(_node + [key]):
                            collect.append((key, deepcopy(_second[key])))
                        else:
                            collect.append((key, _second[key].__class__()))
                            recurred = _diff_recursive(
                                _second[key].__class__(),
                                _second[key],
                                _node=_node + [key],
                            )

                            collect_recurred.append(recurred)

                    if expand:
                        for key, val in collect:
                            yield ADD, dotted_node, [(key, val)]
                    else:
                        yield ADD, dotted_node, collect

                    for recurred in collect_recurred:
                        for diffed in recurred:
                            yield diffed
                else:
                    if expand:
                        for key in addition:
                            yield ADD, dotted_node, [(key, deepcopy(_second[key]))]
                    else:
                        yield ADD, dotted_node, [
                            # for additions, return a list that consist with
                            # two-pair tuples.
                            (key, deepcopy(_second[key]))
                            for key in addition
                        ]

            if deletion:
                assert isinstance(_first, (MutableMapping, MutableSequence))
                if expand:
                    for key in deletion:
                        yield REMOVE, dotted_node, [(key, deepcopy(_first[key]))]
                else:
                    yield REMOVE, dotted_node, [
                        # for deletions, return the list of removed keys
                        # and values.
                        (key, deepcopy(_first[key]))
                        for key in deletion
                    ]

        else:
            # Compare string and numerical types and yield `change` flag.
            if are_different(_first, _second, tolerance):
                yield CHANGE, dotted_node, (deepcopy(_first), deepcopy(_second))

    return _diff_recursive(first, second, node)


def patch(diff_result, destination, in_place=False):
    """Patch the diff result to the destination dictionary.

    :param diff_result: Changes returned by ``diff``.
    :param destination: Structure to apply the changes to.
    :param in_place: By default, destination dictionary is deep copied
                                                                     before applying the patch, and the copy is returned.
                                                                     Setting ``in_place=True`` means that patch will apply
                                                                     the changes directly to and return the destination
                                                                     structure.
    """
    if not in_place:
        destination = deepcopy(destination)

    def add(node, changes):
        for key, value in changes:
            dest = dot_lookup(destination, node)
            if isinstance(dest, MutableSequence):
                dest.insert(key, value)
            elif isinstance(dest, MutableSet):
                dest |= value
            else:
                dest[key] = value

    def change(node, changes):

        dest = dot_lookup(destination, node, parent=True)
        if isinstance(node, str):
            last_node = node.split(".")[-1]
        else:
            last_node = node[-1]
        if isinstance(dest, MutableSequence):
            last_node = int(last_node)
        _, value = changes
        # last_node: Union[str, int]

        dest[last_node] = value  # type:ignore

    def remove(node, changes):
        for key, value in changes:
            dest = dot_lookup(destination, node)
            if isinstance(dest, MutableSet):
                dest -= value
            else:
                del dest[key]

    patchers = {REMOVE: remove, ADD: add, CHANGE: change}

    for action, node, changes in diff_result:
        patchers[action](node, changes)

    return destination


def swap(diff_result):
    """Swap the diff result.

    It uses following mapping:

    - remove -> add
    - add -> remove

    In addition, swap the changed values for `change` flag.

                    >>> from dictdiffer import swap
                    >>> swapped = swap([('add', 'a.b.c', [('a', 'b'), ('c', 'd')])])
                    >>> next(swapped)
                    ('remove', 'a.b.c', [('c', 'd'), ('a', 'b')])

                    >>> swapped = swap([('change', 'a.b.c', ('a', 'b'))])
                    >>> next(swapped)
                    ('change', 'a.b.c', ('b', 'a'))

    """

    def add(node, changes):
        return REMOVE, node, list(reversed(changes))

    def remove(node, changes):
        return ADD, node, changes

    def change(node, changes):
        first, second = changes
        return CHANGE, node, (second, first)

    swappers = {REMOVE: remove, ADD: add, CHANGE: change}

    for action, node, _change in diff_result:
        yield swappers[action](node, _change)


def revert(diff_result, destination, in_place=False):
    """Call swap function to revert patched dictionary object.

    Usage example:

                    >>> from dictdiffer import diff, revert
                    >>> first = {'a': 'b'}
                    >>> second = {'a': 'c'}
                    >>> revert(diff(first, second), second)
                    {'a': 'b'}

    :param diff_result: Changes returned by ``diff``.
    :param destination: Structure to apply the changes to.
    :param in_place: By default, destination dictionary is deep
                                                                     copied before being reverted, and the copy
                                                                     is returned. Setting ``in_place=True`` means
                                                                     that revert will apply the changes directly to
                                                                     and return the destination structure.
    """
    return patch(swap(diff_result), destination, in_place)
