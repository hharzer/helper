import re
import os
import glob

from pathlib import Path
from typing import Union


def change_words(
    search_path: Union[str, os.PathLike, Path],
    file_type: str,
    from_str: Union[str],
    to_str: str,
):
    """
    function to change or replace the text string in files.
    this function also implemented the recursive folders.
    :param `search_path` (String) => is current folder or parent folder for files.
    :param `file_type` (String) => is the file type you are looking for. eg: '.py', '.txt'
    :param `from_str` (String) => is the string to change/replace.
    :param `to_str` (String) => is the string replacement.
    return True
    """

    numb = 0
    for root, dirs, files in os.walk(Path(search_path)):
        for filename in files:
            if filename.endswith(file_type):
                numb += 1

                # finding absolute file path
                # eg: /home/username/tests/findwords/findwords.py
                file_path = os.path.abspath(os.path.join(root, filename))
                print("%s. %s" % (numb, file_path))

                # reading file before replacement
                file = open(file_path, "r")
                file_data = file.read()
                file.close()

                # replace the `from_str` to `to_str` for this file
                new_data = file_data.replace(from_str, to_str)

                # creating new file with same name
                file = open(file_path, "w")
                file.write(new_data)
                file.close()
    # noqa
    return True


import reusables as r


def find_regex(word_pattern: Union[str, re.Pattern], path_to_folder):
    #    os.chdir(path_to_folder)
    regex_find = (
        re.compile(word_pattern) if isinstance(word_pattern, str) else word_pattern
    )
    for filename in r.find_files(ext="*.txt", directory=path_to_folder):
        with open(filename, "r") as file:
            for line in file.readlines():
                get_search = regex_find.findall(line)
                if len(get_search) > 0:
                    print(get_search)
                else:
                    print("No match found")
    return
