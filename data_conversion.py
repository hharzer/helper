from dotenv import dotenv_values
import reusables as r
import difflib as df
from pyprnt import prnt as ls
import pydash as _
from typing import Dict, List, Any
from types import ModuleType

from typing import Callable, Hashable, Literal, Optional, TypeVar, Union, overload
import importlib

# module_to_dict = lambda module:
# {k: getattr(module, k) for k in dir(module) if not k.startswith('_')}
T = TypeVar("T")
DEFAULT_TRANSFORM_KEYS = lambda x: _.to_upper(_.snake_case(x))
DEFAULT_TRANSFORM_VALUE = lambda x: x if not _.is_empty(x) else None


def dict_from_module(
    module: Union[str, ModuleType],
    *,
    ignore_underscore=True,
    only_upper_case=True,
    transform_keys: Union[Literal[False], Callable[[Hashable], Hashable]] = None,
    transform_values: Union[Literal[False], Callable[[T], Optional[T]]] = None,
):
    if not isinstance(module, ModuleType):
        module = importlib.import_module(module)
    if not transform_keys:
        transform_keys = DEFAULT_TRANSFORM_KEYS
    if not transform_values:
        transform_values = DEFAULT_TRANSFORM_VALUE
    context = {}
    for setting in dir(module):
        if (ignore_underscore and not setting.startswith("_")) or (
            only_upper_case and setting.isupper()
        ):
            v = getattr(module, setting)
            if not isinstance(v, ModuleType):
                context[setting if not transform_keys else transform_keys(setting)] = (
                    getattr(module, setting)
                    if not transform_values
                    else transform_values(getattr(module, setting))
                )
    return OrderedDict(context)


def dict_from_py(
    file: Union[str, ModuleType],
    *,
    ignore_underscore=True,
    only_upper_case=True,
    transform_keys: Union[Literal[False], Callable[[Hashable], Hashable]] = None,
    transform_values: Union[Literal[False], Callable[[T], Optional[T]]] = None,
) -> Dict:
    if not isinstance(file, ModuleType):
        file = importlib.import_module(file)
    if not transform_keys:
        transform_keys = DEFAULT_TRANSFORM_KEYS
    if not transform_values:
        transform_values = DEFAULT_TRANSFORM_VALUE

    context = {
        transform_keys(k): transform_values(getattr(file, k))
        for k in dir(file)
        if (
            not ignore_underscore
            and k.startswith("_")
            or (only_upper_case and k.isupper())
        )
    }
    return OrderedDict(context)
