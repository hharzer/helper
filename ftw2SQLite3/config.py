from typing import Any

from pydantic.dataclasses import dataclass
from pydantic import BaseConfig, BaseModel


class Config(BaseModel):
    class Config(BaseConfig):
        orm_mode = True
        allow_population_by_field_name = True
        arbitrary_types_allowed = True

    input_dir: str
    output_dir: str
    read_procs: int
    mmap_size: int
    """def __init__(self, input_dir, output_dir, read_procs, mmap_size):

        self.input_dir = input_dir
        self.output_dir = output_dir

        self.read_procs = read_procs
        self.mmap_size = mmap_size"""
