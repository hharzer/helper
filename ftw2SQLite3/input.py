from typing import Any, Callable, TextIO


def lines(mf: TextIO):
    return iter(mf.readline, b"")


def read_lines(mf: TextIO, f: Callable[[str], Any]):
    # map(lambda g: f(str(g)), lines(mf))
    for g in lines(mf):
        # f(str(g))
        f(str(g).strip())
