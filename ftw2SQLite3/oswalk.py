from os import path, stat, walk, PathLike
from itertools import chain, starmap
from pathlib import Path
from typing import Iterable, List, Union

P = Union[str, PathLike, Path]
# these should have less memory overhead and better response time,
# meaning you can start enqueuing the work tasks sooner
# meaning you can use more cores sooner


def oswalk_helper(root: P, _, files: List[P]):
    return map(lambda name: path.join(root, name), files)


def oswalk(x: Iterable):
    return starmap(oswalk_helper, x)


def oswalk_flatten(input_dir: P):
    return chain.from_iterable(oswalk(walk(input_dir)))


def oswalk_flattens(input_dirs: List[P]):
    return chain.from_iterable(
        map(lambda input_dir: oswalk_flatten(input_dir), input_dirs)
    )


def oswalk_nonempty(input_dir: P):
    return filter(lambda fname: stat(fname).st_size, oswalk_flatten(input_dir))
