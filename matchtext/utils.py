from typing import Optional, Type, TypeVar, Union, overload

from pygments.token import Literal


T = TypeVar("T")
R = TypeVar("R")


@overload
def thisorthat(this: Type[None], that: R) -> R:
    ...


@overload
def thisorthat(this: T, that: R) -> T:
    ...  # type:ignore


def thisorthat(this: Optional[T], that: R) -> Union[T, R]:
    """
    If this is None takes that otherwise takes this.
    :param this:
    :param that:
    :return:
    """
    if this is None:
        return that
    else:
        return this
